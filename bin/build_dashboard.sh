#!/bin/sh
set -eu

# Build dashboard
rm -rf tmp/dashboard
mkdir -p tmp/

cd dashboard
yarn install --frozen-lockfile
yarn generate
cd ..

mv dashboard/dist tmp/dashboard
