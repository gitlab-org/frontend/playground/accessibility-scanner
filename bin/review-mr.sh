#!/usr/bin/env bash
set -euo pipefail

# Example usage:
# bin/review-mr.sh https://gitlab.com/gitlab-renovate-forks/pajamas-adoption-scanner/-/jobs/3291247563

# Some MRs build the dashboard with a different base (e.g., renovate fork MRs),
# so we need to determine it first to serve it correctly.
read_base() {
  grep -o '<base href="[^"]*' public/index.html |
    sed 's!<base href="!!' |
    sed 's!^/*!!' |
    sed 's!/*$!!'
}

PORT=${PORT:-3001}
job_url=${1:-}
zip_path=$(mktemp)
dir=$(mktemp -d)

cd "$dir"

curl --silent --show-error --location --output "$zip_path" "${job_url}/artifacts/download"
unzip "$zip_path" >/dev/null

base=$(read_base)
base_dirname=$(dirname "$base")

mkdir -p "$base_dirname"

mv public "$base"

case "$OSTYPE" in
  linux*)
    xdg-open "http://localhost:$PORT/$base"
    ;;
  darwin*)
    open "http://localhost:$PORT/$base"
    ;;
  *)
    echo "Open http://localhost:$PORT/$base in your browser!"
    ;;
esac

python3 -m http.server "$PORT" --bind localhost --directory . 2>/dev/null >/dev/null
