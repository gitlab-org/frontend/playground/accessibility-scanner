import { fileURLToPath } from 'node:url';
import { readFile } from 'node:fs/promises';
import { join } from 'node:path';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

async function loadPathToGroupMap(filepath) {
  const groupToPaths = JSON.parse(await readFile(filepath, 'utf-8'));
  const duplicates = new Map();
  const pathToGroupMap = Object.entries(groupToPaths).reduce((acc, [group, paths]) => {
    for (const path of paths) {
      if (acc.has(path)) {
        if (!duplicates.has(path)) {
          duplicates.set(path, [acc.get(path)]);
        }

        duplicates.get(path).push(group);
      }
      acc.set(path, group);
    }

    return acc;
  }, new Map());

  return pathToGroupMap;
}

const pathToGroupMap = await loadPathToGroupMap(join(__dirname, 'group_to_paths.json'));

const DEFAULT_GROUP = 'group::unknown';

/**
 * Returns the group label given a file path.
 *
 * It looks up the exact match first, and then lops off path components until
 * a match is found.
 *
 * @param {string} path A file path
 * @returns {string} A group label
 */
export function groupForPath(path) {
  let partialPath = path;

  // eslint-disable-next-line no-constant-condition
  while (true) {
    if (pathToGroupMap.has(partialPath)) return pathToGroupMap.get(partialPath);

    // Have we exhausted all path segments?
    if (partialPath.startsWith('.')) return DEFAULT_GROUP;

    // Lop off another path segment for next iteration.
    partialPath = join(partialPath, '..');
  }
}
