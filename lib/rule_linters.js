const linters = [];

export function lintRules(rules) {
  const errors = [];

  for (const rule of rules) {
    for (const linter of linters) {
      let error;
      try {
        error = linter(rule);
      } catch (e) {
        error = `Linter ${linter.name} failed: ${e.message}`;
      }

      if (error) errors.push(`${rule.id}: ${error}`);
    }
  }

  return errors;
}
